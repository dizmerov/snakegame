package ru.demi.snake;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.layout.GridPane;
import ru.demi.snake.model.BaseCell;
import ru.demi.snake.model.CircleCell;
import ru.demi.snake.model.EmptyCell;
import ru.demi.snake.model.Frog;
import ru.demi.snake.model.Position;
import ru.demi.snake.model.Snake;
import ru.demi.snake.view.CircleCellView;
import ru.demi.snake.view.CompositeView;
import ru.demi.snake.view.EmptyCellView;
import ru.demi.snake.view.View;

/**
 * 
 * Class ViewConverter for converting from model to view
 *
 */
public class ViewConverter {

    /**
     * Convert snake object to cells list
     * @param snake
     * @param grid
     * @return
     */
    public List<BaseCell> convertSnake(Snake snake, GridPane grid) {
        List<BaseCell> cells = new ArrayList<>();
        for (CircleCell snakePart : snake.getParts()) {
            CompositeView composite = new CompositeView();
            List<View> views = composite.getViews();
            views.add(new EmptyCellView(grid, EmptyCell.createByDefault(snakePart.getPosition())));
            views.add(new CircleCellView(grid, snakePart));
            snakePart.setView(composite);
            cells.add(snakePart);
        }

        for (BaseCell oldPart : snake.getOldParts()) {
            CompositeView composite = new CompositeView();
            List<View> views = composite.getViews();
            views.add(new EmptyCellView(grid, EmptyCell.createByDefault(oldPart.getPosition())));
            oldPart.setView(composite);
            cells.add(oldPart);
        }

        return cells;
    }

    /**
     * Convert frog object to cells list
     * @param frog
     * @param grid
     * @return
     */
    public List<BaseCell> convertFrog(Frog frog, GridPane grid) {
        List<BaseCell> cells = new ArrayList<>();
        CompositeView composite = new CompositeView();
        List<View> views = composite.getViews();
        views.add(new EmptyCellView(grid, EmptyCell.createByDefault(frog.getPosition())));
        views.add(new CircleCellView(grid, frog));
        frog.setView(composite);
        cells.add(frog);

        Position pos = frog.getOldPosition();
        if (pos != null) {
            EmptyCell cell = EmptyCell.createByDefault(pos);
            cell.setView(new EmptyCellView(grid, cell));
            cells.add(cell);
        }
        return cells;
    }

    /**
     * Convert box parameters to cells list
     * @param cols
     * @param rows
     * @param grid
     * @return
     */
    public List<BaseCell> convertBoxParams(int cols, int rows, GridPane grid) {
        List<BaseCell> cells = new ArrayList<>();
        for (int x = 0; x < cols; x++) {
            for (int y = 0; y < rows; y++) {
                EmptyCell cell = EmptyCell.createByDefault(new Position(x, y));
                cell.setView(new EmptyCellView(grid, cell));
                cells.add(cell);
            }
        }
        return cells;
    }
}
