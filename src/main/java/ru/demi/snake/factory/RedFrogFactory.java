package ru.demi.snake.factory;

import ru.demi.snake.model.Frog;
import ru.demi.snake.model.Position;
import ru.demi.snake.model.RedFrog;
import ru.demi.snake.strategy.DecreasingEatingStrategy;

/**
 * 
 * Class RedFrogFactory for creating of red frog
 *
 */
public class RedFrogFactory extends FrogFactory {

    @Override
    public Frog createFrog(Position pos) {
        return new RedFrog(pos, Frog.DEFAULT_RADIUS, Frog.DEFAULT_TRANSITION_X, new DecreasingEatingStrategy());
    }
}
