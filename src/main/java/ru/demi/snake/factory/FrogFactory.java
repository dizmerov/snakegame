package ru.demi.snake.factory;

import ru.demi.snake.model.Frog;
import ru.demi.snake.model.Position;

/**
 * 
 * Class FrogFactory for creating of frogs
 *
 */
public abstract class FrogFactory {
    public abstract Frog createFrog(Position pos);
}