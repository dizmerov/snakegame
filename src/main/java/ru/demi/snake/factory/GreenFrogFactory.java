package ru.demi.snake.factory;

import ru.demi.snake.model.Frog;
import ru.demi.snake.model.GreenFrog;
import ru.demi.snake.model.Position;
import ru.demi.snake.strategy.IncreasingEatingStrategy;

/**
 * 
 * Class GreenFrogFactory for creating of green frog
 *
 */
public class GreenFrogFactory extends FrogFactory {

    @Override
    public Frog createFrog(Position pos) {
        return new GreenFrog(pos, Frog.DEFAULT_RADIUS, Frog.DEFAULT_TRANSITION_X, new IncreasingEatingStrategy());
    }
}
