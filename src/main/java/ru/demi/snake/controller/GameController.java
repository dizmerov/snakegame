package ru.demi.snake.controller;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.demi.snake.GameManager;
import ru.demi.snake.ViewConverter;
import ru.demi.snake.exception.NoFreePlacesException;
import ru.demi.snake.model.Event;
import ru.demi.snake.model.Frog;
import ru.demi.snake.model.GameBoard;
import ru.demi.snake.model.Snake;
import ru.demi.snake.view.DialogView;
import ru.demi.snake.view.GameBoardView;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 
 * Class GameController
 *
 */
public class GameController extends Application implements Observer {

    private GameBoardView gameBoardView;
    private Stage stage;
    private GameManager gameManager;
    private ViewConverter viewConverter;

    public Stage getStage() {
        return stage;
    }

    public GameBoardView getGameBoardView() {
        return gameBoardView;
    }

    public void setGameBoardView(GameBoardView gameBoardView) {
        this.gameBoardView = gameBoardView;
    }

    public ViewConverter getViewConverter() {
        return viewConverter;
    }

    public void setViewConverter(ViewConverter viewConverter) {
        this.viewConverter = viewConverter;
    }

    /**
     * Initialization operations
     */
    @Override
    public void init() {
        List<String> args = this.getParameters().getRaw();

        if (args == null || args.size() != 6) {
            String lineSep = System.lineSeparator();
            StringBuilder sb = new StringBuilder(lineSep + "You must pass five arguments:");
            sb.append(lineSep);
            sb.append("\t1 - amount of game board columns,").append(lineSep);
            sb.append("\t2 - amount of game board rows,").append(lineSep);
            sb.append("\t3 - amount of green frogs for game,").append(lineSep);
            sb.append("\t4 - amount of red frogs for game,").append(lineSep);
            sb.append("\t5 - full snake length,").append(lineSep);
            sb.append("\t6 - amount of time for snake sleep in milliseconds.").append(lineSep);
            throw new IllegalArgumentException(sb.toString());
        }

        int cols = Integer.parseInt(args.get(0));
        int rows = Integer.parseInt(args.get(1));
        if (cols < GameBoard.LOWER_BOUND_NUM_CELLS || rows < GameBoard.LOWER_BOUND_NUM_CELLS ||
                cols > GameBoard.UPPER_BOUND_NUM_CELLS || rows > GameBoard.UPPER_BOUND_NUM_CELLS) {
            throw new IllegalArgumentException(
                    String.format("Amount of cols or rows must be positive integer value larger than or equals %d and less than or equals %d.",
                            GameBoard.LOWER_BOUND_NUM_CELLS, GameBoard.UPPER_BOUND_NUM_CELLS));
        }

        int greenFrogsAmount = Integer.parseInt(args.get(2));
        if (greenFrogsAmount < 0) {
            throw new IllegalArgumentException("Amount of green frogs must be positive integer value.");
        }

        int redFrogsAmount = Integer.parseInt(args.get(3));
        if (redFrogsAmount < 0) {
            throw new IllegalArgumentException("Amount of red frogs must be positive integer value.");
        }

        int snakeLength = Integer.parseInt(args.get(4));
        if (snakeLength < 2 || snakeLength >= cols) {
            throw new IllegalArgumentException(
                    "Snake body length must be positive integer value larger or equals 2 and less than number of columns.");
        }

        int snakeSleep = Integer.parseInt(args.get(5));
        if (snakeSleep <= GameBoard.LOWER_BOUND_SNAKE_SLEEP || snakeSleep > GameBoard.UPPER_BOUND_SNAKE_SLEEP) {
            throw new IllegalArgumentException(
                    String.format("Snake sleep must be positive integer value in millis between %d and %d.",
                            GameBoard.LOWER_BOUND_SNAKE_SLEEP, GameBoard.UPPER_BOUND_SNAKE_SLEEP));
        }

        GameBoard gameBoard = new GameBoard(cols, rows);
        gameBoard.setGreenFrogsAmount(greenFrogsAmount);
        gameBoard.setRedFrogsAmount(redFrogsAmount);
        gameBoard.setSnakeLength(snakeLength);
        gameBoard.setSnakeSleep(snakeSleep);
        gameBoard.setFrogSleep(snakeSleep * GameBoard.SLEEP_FACTOR);
        gameBoard.computeSceneWidth();
        gameBoard.computeSceneHeight();
        gameManager = new GameManager(gameBoard);
        gameManager.addObserver(this);
        this.viewConverter = new ViewConverter();
        gameBoard.setNumFreePlaces(cols * rows);
    }

    /**
     * Start action
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            this.stage = primaryStage;
            this.gameBoardView = new GameBoardView(this);
            GameBoard gameBoard = gameManager.getGameBoard();
            gameBoardView.init(gameBoard.getCols(), gameBoard.getRows(), gameBoard.getSceneWidth(),
                    gameBoard.getSceneHeight());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handler for start button clicks
     */
    public void onBtnStartClick() {

        try {
            gameManager.startGame(this);
        } catch (NoFreePlacesException e) {
            new DialogView(this).render();
            gameManager.stopGame();
        }
    }

    /**
     * Handler for stop button clicks
     */
    public void onBtnStopClick() {
        gameManager.stopGame();
    }

    /**
     * Handler for pause button clicks
     */
    public void onBtnPauseClick() {
        gameManager.pauseGame();
    }

    /**
     * Handler for left mouse button clicks
     */
    public void onLeftBtnMouseClick() {
        gameManager.directSnake(-1);
    }

    /**
     * Handler for right mouse button clicks
     */
    public void onRightBtnMouseClick() {
        gameManager.directSnake(1);
    }

    /**
     * Start application method
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Reset points action
     */
    public void resetPoints() {
        gameManager.resetPoints();
        gameBoardView.renderPoints(GameBoard.points);
    }

    /**
     * Reaction for model updating
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Snake) {
            Snake snake = (Snake) arg;
            gameManager.moveSnake(snake, this);
            gameBoardView.render(viewConverter.convertSnake(snake, gameBoardView.getGrid()));
        } else if (arg instanceof Frog) {
            Frog frog = (Frog) arg;
            gameManager.moveFrog(frog);
            gameBoardView.render(viewConverter.convertFrog(frog, gameBoardView.getGrid()));
        } else if (arg instanceof Event  ) {
            Event event = (Event) arg;
            if (event == Event.SHOW_POINTS) {
                gameBoardView.renderPoints(GameBoard.points);
            } else if (event == Event.START_GAME) {
                GameBoard gameBoard = gameManager.getGameBoard();
                gameBoardView.render(viewConverter.convertBoxParams(gameBoard.getCols(), gameBoard.getRows(),
                        gameBoardView.getGrid()));
                gameBoardView.render(viewConverter.convertSnake(gameBoard.getSnake(), gameBoardView.getGrid()));
                for (Frog frog : gameBoard.getFrogs()) {
                    gameBoardView.render(viewConverter.convertFrog(frog, gameBoardView.getGrid()));
                }
                new Thread(() -> {
                    try {
                        Thread.sleep(gameBoard.getSnakeSleep());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    gameManager.startThreads();
                }).start();
            }
        }
    }
}
