package ru.demi.snake;

import ru.demi.snake.model.Direction;
import ru.demi.snake.model.Position;

/**
 *
 * Class PositionHelper for helper actions with position and direction
 *
 */
public class PositionHelper {

    /**
     * Return next position
     * @param direction
     * @param pos
     * @param cols
     * @param rows
     * @return
     */
    public static Position getNextPosition(Direction direction, Position pos, int cols, int rows) {
        int x = pos.getX();
        int y = pos.getY();
        switch (direction) {
            case LEFT:
                --x;
                break;
            case RIGHT:
                ++x;
                break;
            default:
                break;
        }
        x = x % cols;
        if (x < 0) {
            x += cols;
        }

        switch (direction) {
            case TOP:
                --y;
                break;
            case DOWN:
                ++y;
                break;
            default:
                break;
        }
        y = y % rows;
        if (y < 0) {
            y += rows;
        }

        return new Position(x, y);
    }

    /**
     * Return next direction
     * @param direction
     * @param diff
     * @return
     */
    public static Direction getNextDirection(Direction direction, int diff) {
        if (diff == 0) {
            return direction;
        }

        Direction[] dirs = Direction.values();
        int pos = direction.ordinal();
        pos = (pos + diff) % dirs.length;

        if (pos < 0) {
            pos += dirs.length;
        }
        return dirs[pos];
    }
}
