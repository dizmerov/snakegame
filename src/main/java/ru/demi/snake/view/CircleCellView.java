package ru.demi.snake.view;

import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import ru.demi.snake.model.CircleCell;

/**
 * 
 * Class CircleCellView
 *
 */
public class CircleCellView extends View {

    private CircleCell cell;

    public CircleCellView(GridPane grid, CircleCell cell) {
        super(grid);
        this.cell = cell;
    }

    @Override
    public void render() {
        Circle c = new Circle(cell.getRadius(), cell.getColor());
        c.setTranslateX(cell.getTranslationX());
        getGrid().add(c, cell.getPosition().getX(), cell.getPosition().getY());
    }
}
