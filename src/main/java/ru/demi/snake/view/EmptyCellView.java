package ru.demi.snake.view;

import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import ru.demi.snake.model.EmptyCell;

/**
 * 
 * Class EmptyCellView
 *
 */
public class EmptyCellView extends View {

    private EmptyCell cell;

    public EmptyCellView(GridPane grid, EmptyCell cell) {
        super(grid);
        this.cell = cell;
    }

    @Override
    public void render() {
        Rectangle r = new Rectangle(cell.getWidth(), cell.getHeight());
        r.setFill(cell.getFillColor());
        r.setStroke(cell.getStrokeColor());
        grid.add(r, cell.getPosition().getX(), cell.getPosition().getY());
    }
}
