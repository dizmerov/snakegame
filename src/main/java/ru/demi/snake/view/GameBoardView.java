package ru.demi.snake.view;

import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ru.demi.snake.controller.GameController;
import ru.demi.snake.model.BaseCell;

/**
 * Base view for game
 * Class GameBoardView
 *
 */
public class GameBoardView {

    private GridPane grid;
    private GameController gameController;
    private HBox labelsBox;

    public GameBoardView(GameController controller) {
        gameController = controller;
        gameController.getStage().setTitle("Snake game");
    }

    public GridPane getGrid() {
        return grid;
    }

    public void setGrid(GridPane grid) {
        this.grid = grid;
    }

    public HBox getLabelsBox() {
        return labelsBox;
    }

    public void setLabelsBox(HBox labelsBox) {
        this.labelsBox = labelsBox;
    }

    private GridPane buildGrid() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setGridLinesVisible(true);
        return grid;
    }

    private HBox buildBtnBox() {
        Button btnStart = new Button("Start");
        btnStart.setPrefSize(100, 20);
        btnStart.setOnAction(event -> gameController.onBtnStartClick());

        Button btnPause = new Button("Pause");
        btnPause.setPrefSize(100, 20);
        btnPause.setOnAction(event -> gameController.onBtnPauseClick());

        Button btnStop = new Button("Stop");
        btnStop.setPrefSize(100, 20);
        btnStop.setOnAction(event -> gameController.onBtnStopClick());

        HBox btnBox = new HBox(10);
        btnBox.setPadding(new Insets(15, 10, 30, 10));
        btnBox.setAlignment(Pos.CENTER);
        btnBox.getChildren().addAll(btnStart, btnPause, btnStop);
        return btnBox;
    }

    private HBox buildLabelsBox() {
        HBox box = new HBox();
        box.setPadding(new Insets(20, 10, 0, 10));
        box.setAlignment(Pos.CENTER);
        Label labelText = new Label("Points:");
        Label labelPoints = new Label();
        box.getChildren().addAll(labelText, labelPoints);
        return box;
    }

    private void initDirectionHandlers(GridPane grid) {
        grid.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                gameController.onLeftBtnMouseClick();
            } else if (event.getButton() == MouseButton.SECONDARY) {
                gameController.onRightBtnMouseClick();
            }

        });
    }

    public void init(int cols, int rows, int width, int height) {

        BorderPane borderPane = new BorderPane();
        grid = buildGrid();
        borderPane.setCenter(grid);
        VBox box = new VBox();
        HBox labelsBox = buildLabelsBox();
        setLabelsBox(labelsBox);
        box.getChildren().addAll(labelsBox, buildBtnBox());
        borderPane.setBottom(box);
        renderPoints(0);

        render(gameController.getViewConverter().convertBoxParams(cols, rows, grid));
        Scene scene = new Scene(borderPane, width, height);
        initDirectionHandlers(grid);
        gameController.getStage().setScene(scene);
        gameController.getStage().show();
    }

    public void renderPoints(int points) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Label label = (Label) getLabelsBox().getChildren().get(1);
                label.setText(String.valueOf(points));
            }
        });
    }

    public void render(List<BaseCell> cells) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                cells.forEach(cell -> cell.getView().render());
            }
        });
    }
}
