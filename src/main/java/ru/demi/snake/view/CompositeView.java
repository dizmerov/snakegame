package ru.demi.snake.view;

import java.util.ArrayList;
import java.util.List;

/**
 * Parted realization of Composite pattern
 * Class CompositeView
 *
 */
public class CompositeView extends View {

    protected List<View> views = new ArrayList<>();

    @Override
    public void render() {
        views.forEach(view -> view.render());
    }

    public List<View> getViews() {
        return views;
    }

    public void setViews(List<View> views) {
        this.views = views;
    }
}
