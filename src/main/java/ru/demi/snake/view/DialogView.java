package ru.demi.snake.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.demi.snake.controller.GameController;

/**
 * @author demi
 * @date 02.06.16
 */
public class DialogView extends View {

    private final GameController gameController;

    public DialogView(GameController controller) {
        gameController = controller;
    }

    @Override
    public void render() {
        final Stage dialog = new Stage(StageStyle.DECORATED);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(gameController.getStage());

        Label labelText = new Label("There are no free places for game object. Game is stopped.");

        Button btnOk = new Button("Ok");
        btnOk.setPrefSize(100, 20);
        btnOk.setOnAction(event -> {
            gameController.getStage().getScene().getRoot().setEffect(null);
            dialog.close();
        });

        VBox box = new VBox(10);
        box.setPadding(new Insets(20, 20, 20, 20));
        box.setAlignment(Pos.CENTER);
        box.getChildren().addAll(labelText, btnOk);

        dialog.setScene(new Scene(box));

        // allow the dialog to be dragged around.
        final Node root = dialog.getScene().getRoot();
        final Delta dragDelta = new Delta();
        root.setOnMousePressed(mouseEvent -> {
            // record a delta distance for the drag and drop operation.
            dragDelta.x = dialog.getX() - mouseEvent.getScreenX();
            dragDelta.y = dialog.getY() - mouseEvent.getScreenY();
        });
        root.setOnMouseDragged(mouseEvent -> {
            dialog.setX(mouseEvent.getScreenX() + dragDelta.x);
            dialog.setY(mouseEvent.getScreenY() + dragDelta.y);
        });
        dialog.show();
    }

    // records relative x and y co-ordinates.
    private class Delta { double x, y; }
}
