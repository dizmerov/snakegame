package ru.demi.snake.view;

import javafx.scene.layout.GridPane;

/**
 * 
 * Class View
 *
 */
public abstract class View {

    protected GridPane grid;

    public View() {
    }

    public View(GridPane grid) {
        this.grid = grid;
    }

    abstract public void render();

    public GridPane getGrid() {
        return grid;
    }

    public void setGrid(GridPane grid) {
        this.grid = grid;
    }

}
