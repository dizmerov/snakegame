package ru.demi.snake.threads;

import java.util.Observable;
import ru.demi.snake.model.GameBoard;
import ru.demi.snake.model.Snake;

/**
 * 
 * Class SnakeThread
 *
 */
public class SnakeThread extends Observable implements Runnable {

    private Snake snake;
    private int snakeSleep;

    public SnakeThread(Snake snake, int snakeSleep) {
        this.snake = snake;
        this.snakeSleep = snakeSleep;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted() && !GameBoard.isStopped) {
                setChanged();
                notifyObservers(snake);
                Thread.sleep(snakeSleep);
            }
        } catch (Exception e) {
            System.out.println("Snake " + this + " is died.");
        }
    }
}