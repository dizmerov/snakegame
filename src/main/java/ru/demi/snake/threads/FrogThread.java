package ru.demi.snake.threads;

import java.util.Observable;
import ru.demi.snake.model.Frog;
import ru.demi.snake.model.GameBoard;

/**
 * 
 * Class FrogThread
 *
 */
public class FrogThread extends Observable implements Runnable {

    private Frog frog;
    private int frogSleep;

    public FrogThread(Frog frog, int frogSleep) {
        this.frog = frog;
        this.frogSleep = frogSleep;
    }

    public void run() {
        try {
            while (!GameBoard.isStopped && !Thread.currentThread().isInterrupted()) {
                setChanged();
                notifyObservers(frog);
                Thread.sleep(frogSleep);
            }
        } catch (InterruptedException e) {
            System.out.println("Frog " + this + " is died.");
        }
    }
}