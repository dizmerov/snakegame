package ru.demi.snake.model;

import ru.demi.snake.view.View;

/**
 * 
 * Class BaseCell is base class for cells
 *
 */
public abstract class BaseCell {

    private Position position;
    private View view;

    public BaseCell(Position pos) {
        position = pos;
    }

    public Position getPosition() {
        return new Position(position.getX(), position.getY());
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}
