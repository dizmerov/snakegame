package ru.demi.snake.model;

import javafx.scene.paint.Color;

/**
 * 
 * Class EmptyCell
 *
 */
public class EmptyCell extends BaseCell {
    private int width;
    private int height;
    private Color fillColor;
    private Color strokeColor;

    private static final Color DEFAULT_FILL_COLOR = Color.BLACK;
    private static final Color DEFAULT_STROKE_COLOR = Color.WHITE;

    private EmptyCell(Position pos) {
        super(pos);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public Color getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
    }

    public static EmptyCell of(int width, int height, Color fillColor, Color strokeColor, Position pos) {
        EmptyCell cell = new EmptyCell(pos);
        cell.setWidth(width);
        cell.setHeight(height);
        cell.setFillColor(fillColor);
        cell.setStrokeColor(strokeColor);
        return cell;
    }

    public static EmptyCell createByDefault(Position pos) {
        EmptyCell cell = new EmptyCell(pos);
        cell.setWidth(GameBoard.CELL_SIZE);
        cell.setHeight(GameBoard.CELL_SIZE);
        cell.setFillColor(DEFAULT_FILL_COLOR);
        cell.setStrokeColor(DEFAULT_STROKE_COLOR);
        return cell;
    }
}
