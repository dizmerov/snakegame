package ru.demi.snake.model;

import javafx.scene.paint.Color;
import ru.demi.snake.factory.FrogFactory;
import ru.demi.snake.factory.RedFrogFactory;
import ru.demi.snake.strategy.EatingStrategy;

/**
 * 
 * Class RedFrog
 *
 */
public class RedFrog extends Frog {

    public static final FrogFactory FACTORY = new RedFrogFactory();

    public RedFrog(Position pos, double radius, double translationX, EatingStrategy snakeEatingStrategy) {
        super(pos, Color.RED, radius, translationX, snakeEatingStrategy);
    }

    @Override
    public FrogFactory getFactory() {
        return FACTORY;
    }
}
