package ru.demi.snake.model;

import ru.demi.snake.exception.NoFreePlacesException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * Class GameBoard
 *
 */
public class GameBoard {

    /**
     * Parameters of game
     */
    private int cols;
    private int rows;
    private int greenFrogsAmount;
    private int redFrogsAmount;
    private int snakeLength;
    private int snakeSleep;
    private int frogSleep;
    private int sceneWidth = 400;
    private int sceneHeight = 400;
    private int numFreePlaces;
    public static final int LOWER_BOUND_NUM_CELLS = 10;
    public static final int UPPER_BOUND_NUM_CELLS = 20;
    public static final int LOWER_BOUND_SNAKE_SLEEP = 300;
    public static final int UPPER_BOUND_SNAKE_SLEEP = 3000;
    public static final int SLEEP_FACTOR = 2;
    public static final int CELL_SIZE = 30;

    /**
     * State of game
     */
    private Snake snake;
    private List<Frog> frogs = new ArrayList<>();
    public static boolean isStarted = false;
    public static boolean isStopped = true;
    public static boolean isPaused = false;
    public static int points = 0;

    public GameBoard(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public int getGreenFrogsAmount() {
        return greenFrogsAmount;
    }

    public void setGreenFrogsAmount(int greenFrogsAmount) {
        this.greenFrogsAmount = greenFrogsAmount;
    }

    public int getRedFrogsAmount() {
        return redFrogsAmount;
    }

    public void setRedFrogsAmount(int redFrogsAmount) {
        this.redFrogsAmount = redFrogsAmount;
    }

    public int getSnakeLength() {
        return snakeLength;
    }

    public void setSnakeLength(int snakeLength) {
        this.snakeLength = snakeLength;
    }

    public int getSnakeSleep() {
        return snakeSleep;
    }

    public void setSnakeSleep(int snakeSleep) {
        this.snakeSleep = snakeSleep;
    }

    public int getFrogSleep() {
        return frogSleep;
    }

    public void setFrogSleep(int frogSleep) {
        this.frogSleep = frogSleep;
    }

    public int getSceneWidth() {
        return sceneWidth;
    }

    public int getSceneHeight() {
        return sceneHeight;
    }

    public Snake getSnake() {
        return snake;
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }

    public List<Frog> getFrogs() {
        return frogs;
    }

    public void setFrogs(List<Frog> frogs) {
        this.frogs = frogs;
    }

    public int getNumFreePlaces() {
        return numFreePlaces;
    }

    public void setNumFreePlaces(int numFreePlaces) {
        this.numFreePlaces = numFreePlaces;
    }

    /**
     * Does the object with passed position intersect the snake or any frogs?
     * @param pos
     * @return
     */
    public boolean intersectsOthers(Position pos) {
        return intersectsAnyFrog(pos) || intersectsSnake(pos);
    }

    /**
     * Does the object with passed position intersect any frogs?
     * @param pos
     * @return
     */
    public boolean intersectsAnyFrog(Position pos) {
        return getIntersectedFrog(pos) != null;
    }

    /**
     * Does the object with passed position intersect the snake?
     * @param pos
     * @return
     */
    public boolean intersectsSnake(Position pos) {
        for (SnakePart part : snake.getParts()) {
            if (part.getPosition().equals(pos)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return the frog in intersected position
     * @param pos
     * @return
     */
    public Frog getIntersectedFrog(Position pos) {
        for (Frog frog : frogs) {
            if (frog.getPosition().equals(pos)) {
                return frog;
            }
        }
        return null;
    }

    /**
     * Is the object with passed position intersect the borders
     * @param pos
     * @return
     */
    public boolean intersectsBorders(Position pos) {
        int x = pos.getX();
        int y = pos.getY();
        return !((x >= 0 && x < getCols()) && (y >= 0 && y < getRows()));
    }

    /**
     * Return the random position
     * @return
     */
    public Position getRandomPosition() {

        if (numFreePlaces == 0) {
            throw new NoFreePlacesException();
        }

        int randX = 0;
        int randY = 0;
        Random random = new Random();
        while (true) {
            int x = random.nextInt(getCols());
            int y = random.nextInt(getRows());
            if (!intersectsOthers(new Position(x, y))) {
                randX = x;
                randY = y;
                break;
            }
        }
        return new Position(randX, randY);
    }

    /**
     * Compute the width of scene
     */
    public void computeSceneWidth() {
        sceneWidth = cols * CELL_SIZE;
        sceneWidth += 100;
    }

    /**
     * Compute the height of scene
     */
    public void computeSceneHeight() {
        sceneHeight = rows * CELL_SIZE;
        sceneHeight += 100;
    }

    public void increaseFreePlaces(int amount) {
        numFreePlaces += amount;
    }

    public void decreaseFreePlaces(int amount) {
        numFreePlaces -= amount;
    }
}
