package ru.demi.snake.model;

import javafx.scene.paint.Color;

/**
 * 
 * Class CircleCell is circle figure in cell
 *
 */
public class CircleCell extends BaseCell {
    private Color color;
    private double radius;
    private double translationX;

    public CircleCell(Position pos, Color color, double radius, double translationX) {
        super(pos);
        this.color = color;
        this.radius = radius;
        this.translationX = translationX;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getTranslationX() {
        return translationX;
    }

    public void setTranslationX(double translationX) {
        this.translationX = translationX;
    }
}
