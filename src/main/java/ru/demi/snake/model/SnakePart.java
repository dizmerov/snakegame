package ru.demi.snake.model;

import javafx.scene.paint.Color;

/**
 * 
 * Class SnakePart is a part of the snake
 *
 */
public class SnakePart extends CircleCell {

    private SnakePart(Position pos, Color color, double radius, double translationX) {
        super(pos, color, radius, translationX);
    }

    public static SnakePart asBody(Position pos) {
        return new SnakePart(pos, Color.YELLOW, ((double) GameBoard.CELL_SIZE) / 3, ((double) GameBoard.CELL_SIZE) / 6);
    }

    public static SnakePart asHead(Position pos) {
        return new SnakePart(pos, Color.YELLOW, ((double) GameBoard.CELL_SIZE) / 2, 0);
    }

    public static SnakePart asTail(Position pos) {
        return new SnakePart(pos, Color.YELLOW, ((double) GameBoard.CELL_SIZE) / 4, ((double) GameBoard.CELL_SIZE) / 5);
    }
}
