package ru.demi.snake.model;

/**
 * 
 * Enumeration Event
 *
 */
public enum Event {

    START_GAME, SHOW_POINTS;
}
