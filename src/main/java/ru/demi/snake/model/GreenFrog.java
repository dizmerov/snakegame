package ru.demi.snake.model;

import javafx.scene.paint.Color;
import ru.demi.snake.factory.FrogFactory;
import ru.demi.snake.factory.GreenFrogFactory;
import ru.demi.snake.strategy.EatingStrategy;

/**
 * 
 * Class GreenFrog
 *
 */
public class GreenFrog extends Frog {

    public static final FrogFactory FACTORY = new GreenFrogFactory();

    public GreenFrog(Position pos, double radius, double translationX, EatingStrategy snakeEatingStrategy) {
        super(pos, Color.GREEN, radius, translationX, snakeEatingStrategy);
    }

    @Override
    public FrogFactory getFactory() {
        return FACTORY;
    }
}