package ru.demi.snake.model;

import ru.demi.snake.PositionHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Class Snake
 *
 */
public class Snake {

    private Direction direction = Direction.RIGHT;
    private List<SnakePart> parts = new ArrayList<>();
    private List<SnakePart> oldParts = new ArrayList<>();

    public Snake(Position pos, int length) {

        int x = pos.getX();
        int y = pos.getY();
        parts.add(SnakePart.asHead(new Position(x + length - 1, y)));
        for (int i = x + length - 2; i > x; i--) {
            parts.add(SnakePart.asBody(new Position(i, y)));
        }
        parts.add(SnakePart.asTail(new Position(x, y)));
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public List<SnakePart> getParts() {
        return parts;
    }

    public void setParts(List<SnakePart> parts) {
        this.parts = parts;
    }

    public List<SnakePart> getOldParts() {
        return oldParts;
    }

    public void setOldParts(List<SnakePart> oldParts) {
        this.oldParts = oldParts;
    }

    public Position getPosition() {
        return parts.get(0).getPosition();
    }

    public Position getNextPosition(int cols, int rows) {
        return PositionHelper.getNextPosition(direction, getPosition(), cols, rows);
    }

    /**
     * Set next direction by passed difference
     * @param diff
     */
    public void directByDiff(int diff) {
        setDirection(PositionHelper.getNextDirection(getDirection(), diff));
    }

    /**
     * Run eating logic by eatable object in passed position
     * @param eatable
     * @param pos
     */
    public void eat(Eatable eatable, Position pos) {
        eatable.getEatingStrategy().eat(this, pos);
    }

    /**
     * Make next step
     * @param cols
     * @param rows
     */
    public void makeStep(int cols, int rows) {
        SnakePart head = parts.get(0);
        Position prevPosition = head.getPosition();
        head.setPosition(getNextPosition(cols, rows));

        SnakePart tail = parts.get(parts.size() - 1);
        oldParts.clear();
        oldParts.add(SnakePart.asBody(tail.getPosition()));
        if (parts.size() > 2) {
            parts.add(0, head);
            parts.set(1, SnakePart.asBody(prevPosition));
            parts.remove(parts.size() - 1);
            SnakePart lastBodyPart = parts.get(parts.size() - 1);
            tail.setPosition(lastBodyPart.getPosition());
            parts.set(parts.size() - 1, tail);
        } else {
            tail.setPosition(prevPosition);
        }
    }
}
