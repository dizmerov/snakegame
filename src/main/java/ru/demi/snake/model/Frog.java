package ru.demi.snake.model;

import javafx.scene.paint.Color;
import ru.demi.snake.factory.FrogFactory;
import ru.demi.snake.strategy.EatingStrategy;

/**
 * 
 * Class Frog
 *
 */
public abstract class Frog extends CircleCell implements Eatable {

    private Direction direction;
    private Position oldPosition;
    private long id;
    private EatingStrategy eatingStrategy;
    public static final double DEFAULT_RADIUS = ((double) GameBoard.CELL_SIZE) / 3;
    public static final double DEFAULT_TRANSITION_X = ((double) GameBoard.CELL_SIZE) / 6;

    public Frog(Position pos, Color color, double radius, double translationX, EatingStrategy eatingStrategy) {
        super(pos, color, radius, translationX);
        this.eatingStrategy = eatingStrategy;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction dir) {
        this.direction = dir;
    }

    public Position getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(Position oldPosition) {
        this.oldPosition = oldPosition;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public EatingStrategy getEatingStrategy() {
        return eatingStrategy;
    }

    public void setEatingStrategy(EatingStrategy eatingStrategy) {
        this.eatingStrategy = eatingStrategy;
    }

    public abstract FrogFactory getFactory();

}
