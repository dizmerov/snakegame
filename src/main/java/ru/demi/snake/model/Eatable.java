package ru.demi.snake.model;

import ru.demi.snake.strategy.EatingStrategy;

/**
 * 
 * Implemented by classes objects that the snake can eat
 *
 */
public interface Eatable {
    EatingStrategy getEatingStrategy();
}
