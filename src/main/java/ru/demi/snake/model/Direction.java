package ru.demi.snake.model;

/**
 * 
 * Enumeration Direction
 *
 */
public enum Direction {
    TOP, RIGHT, DOWN, LEFT
}
