package ru.demi.snake.strategy;

import java.util.List;

import ru.demi.snake.model.GameBoard;
import ru.demi.snake.model.Position;
import ru.demi.snake.model.Snake;
import ru.demi.snake.model.SnakePart;

/**
 * Logic of decreasing of the snake
 * Class DecreasingEatingStrategy
 *
 */
public class DecreasingEatingStrategy implements EatingStrategy {

    @Override
    public void eat(Snake snake, Position pos) {
        List<SnakePart> parts = snake.getParts();
        SnakePart head = parts.get(0);
        Position prevHeadPosition = head.getPosition();
        head.setPosition(pos);

        SnakePart tail = parts.get(parts.size() - 1);
        List<SnakePart> oldParts = snake.getOldParts();
        oldParts.clear();
        oldParts.add(SnakePart.asBody(tail.getPosition()));
        if (parts.size() >= 3) {
            SnakePart lastBodyPart = parts.get(parts.size() - 2);
            oldParts.add(SnakePart.asBody(lastBodyPart.getPosition()));

            parts.add(0, head);
            parts.set(1, SnakePart.asBody(prevHeadPosition));
            parts.remove(parts.size() - 1);
            parts.remove(parts.size() - 1);
            tail.setPosition(parts.get(parts.size() - 1).getPosition());
            parts.set(parts.size() - 1, tail);
        } else {
            tail.setPosition(prevHeadPosition);
        }
        GameBoard.points += 2;
    }
}
