package ru.demi.snake.strategy;

import ru.demi.snake.model.Position;
import ru.demi.snake.model.Snake;

/**
 * 
 * Interface EatingStrategy
 *
 */
public interface EatingStrategy {
    void eat(Snake snake, Position pos);
}