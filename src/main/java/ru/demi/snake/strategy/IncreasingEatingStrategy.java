package ru.demi.snake.strategy;

import java.util.ArrayList;
import java.util.List;

import ru.demi.snake.model.GameBoard;
import ru.demi.snake.model.Position;
import ru.demi.snake.model.Snake;
import ru.demi.snake.model.SnakePart;

/**
 * Logic of increasing of the snake
 * Class IncreasingEatingStrategy
 *
 */
public class IncreasingEatingStrategy implements EatingStrategy {

    @Override
    public void eat(Snake snake, Position pos) {
        List<SnakePart> parts = snake.getParts();
        SnakePart head = parts.get(0);
        parts.set(0, SnakePart.asBody(head.getPosition()));
        head.setPosition(pos);
        parts.add(0, head);
        snake.setOldParts(new ArrayList<>());
        ++GameBoard.points;
    }
}
