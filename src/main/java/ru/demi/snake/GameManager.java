package ru.demi.snake;

import ru.demi.snake.controller.GameController;
import ru.demi.snake.factory.FrogFactory;
import ru.demi.snake.model.*;
import ru.demi.snake.threads.FrogThread;
import ru.demi.snake.threads.SnakeThread;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class GameManager contains main logic methods for game managing 
 */
public class GameManager extends Observable {

    private GameBoard gameBoard;
    private Thread snakeThread;
    private Map<Long, Thread> frogThreads = new HashMap<>();

    public GameManager(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }

    /**
     * Create snake object for game
     * @param observer
     */
    private void createSnake(Observer observer) {
        Snake snake = new Snake(new Position(0, 0), gameBoard.getSnakeLength());
        getGameBoard().setSnake(snake);
        createSnakeThread(observer, snake);
    }

    /**
     * Create thread for generation of movement events of snake
     * @param observer
     * @param snake
     */
    private void createSnakeThread(Observer observer, Snake snake) {
        SnakeThread snakeThread = new SnakeThread(snake, gameBoard.getSnakeSleep());
        snakeThread.addObserver(observer);

        Thread thread = new Thread(snakeThread);
        thread.setDaemon(true);
        this.snakeThread = thread;
    }

    /**
     * Create frogs objects for game
     * @param observer
     * @param factory
     * @param frogsAmount
     */
    private void createFrogs(Observer observer, FrogFactory factory, int frogsAmount) {
        for (int i = 0, l = frogsAmount; i < l; i++) {
            createFrog(factory, observer);
        }
    }
    
    /**
     * Create threads for frog objects
     * @param observer
     * @param frogs
     */
    private void createFrogThreads(Observer observer, List<Frog> frogs) {
        for (Frog frog : frogs) {
            createFrogThread(observer, frog);
        }
    }

    /**
     * Create objects for green frogs
     * @param observer
     */
    private void createGreenFrogs(Observer observer) {
        createFrogs(observer, GreenFrog.FACTORY, gameBoard.getGreenFrogsAmount());
    }

    /**
     * Create objects for red frogs
     * @param observer
     */
    private void createRedFrogs(Observer observer) {
        createFrogs(observer, RedFrog.FACTORY, gameBoard.getRedFrogsAmount());
    }

   /**
    * Create frog object
    * @param factory
    * @param observer
    * @return frog
    */
    private Frog createFrog(FrogFactory factory, Observer observer) {
        Position pos = gameBoard.getRandomPosition();
        Frog frog = factory.createFrog(pos);
        gameBoard.getFrogs().add(frog);

        createFrogThread(observer, frog);
        gameBoard.decreaseFreePlaces(1);
        return frog;
    }

    /**
     * Create thread for movement events generation of frogs
     * @param observer
     * @param frog
     */
    private void createFrogThread(Observer observer, Frog frog) {
        FrogThread frogThread = new FrogThread(frog, gameBoard.getFrogSleep());
        frogThread.addObserver(observer);
        Thread thread = new Thread(frogThread);
        thread.setDaemon(true);
        frog.setId(thread.getId());
        frogThreads.put(thread.getId(), thread);
    }

    /**
     * Set direction to snake by passed difference
     * @param diff
     */
    public void directSnake(int diff) {
        synchronized (gameBoard) {
            Snake snake = gameBoard.getSnake();
            snake.directByDiff(diff);
        }
    }

    /**
     * Start threads
     */
    public void startThreads() {
        startSnakeThread();
        startFrogThreads();
    }

    /**
     * Start snake thread
     */
    private void startSnakeThread() {
        snakeThread.start();
    }

    /**
     * Start frog threads
     */
    private void startFrogThreads() {
        frogThreads.values().forEach(thread -> thread.start());
    }

    /**
     * Start frog thread by passed id
     * @param id
     */
    private void startFrogThread(long id) {
        frogThreads.get(id).start();
    }

    /**
     * Finish threads
     */
    private void stopThreads() {
        gameBoard.increaseFreePlaces(gameBoard.getSnakeLength());
        snakeThread.interrupt();
        gameBoard.increaseFreePlaces(frogThreads.size());
        frogThreads.values().forEach(thread -> thread.interrupt());
        frogThreads.clear();
    }

    /**
     * Increments points of game
     */
    public void incrementPoints() {
        ++GameBoard.points;
    }

    /**
     * Reset points of game to zero
     */
    public void resetPoints() {
        GameBoard.points = 0;
    }

    /**
     * Move snake to next position
     * @param snake
     * @param observer
     */
    public void moveSnake(Snake snake, Observer observer) {
        Position pos = snake.getNextPosition(gameBoard.getCols(), gameBoard.getRows());
        synchronized (gameBoard) {
            if (gameBoard.intersectsSnake(pos)) {
                stopGame();
                return;
            }

            Frog frog = gameBoard.getIntersectedFrog(pos);
            if (frog != null) {
                feedSnake(snake, frog, gameBoard.getFrogs());
                Frog createdFrog = createFrog(frog.getFactory(), observer);
                startFrogThread(createdFrog.getId());

                setChanged();
                notifyObservers(Event.SHOW_POINTS);
            } else {
                snake.makeStep(gameBoard.getCols(), gameBoard.getRows());
            }
        }
    }

    /**
     * Feed snake by passed frog
     * @param snake
     * @param frog
     * @param frogs
     */
    private void feedSnake(Snake snake, Frog frog, List<Frog> frogs) {
        snake.eat(frog, frog.getPosition());
        frog.setOldPosition(null);
        frogThreads.get(frog.getId()).interrupt();
        frogs.remove(frog);
        gameBoard.decreaseFreePlaces(1);
    }

    /**
     * Try to move the frog to next position
     * @param frog
     */
    public void moveFrog(Frog frog) {

        boolean isMoved = false;
        synchronized (gameBoard) {
            if (gameBoard.getNumFreePlaces() == 0) {
                return;
            }

            Position oldPosition = frog.getPosition();
            List<Direction> directions = new ArrayList<>(Arrays.asList(Direction.values()));
            directions = directions.stream()
                    .filter(dir -> dir != frog.getDirection())
                    .collect(Collectors.toList());

            Random random = new Random();
            while (directions.size() > 0) {
                int rand = random.nextInt(directions.size());
                Position nextPos = PositionHelper.getNextPosition(directions.get(rand), frog.getPosition(), gameBoard.getCols(), gameBoard.getRows());
                if (!gameBoard.intersectsOthers(nextPos)) {
                    frog.setDirection(directions.get(rand));
                    frog.setPosition(nextPos);
                    isMoved = true;
                    break;
                }
                directions.remove(rand);
            }

            if (isMoved) {
                frog.setOldPosition(oldPosition);
            }
        }
    }

    /**
     * Make initialization operations for game start 
     * @param gameController
     */
    public void startGame(GameController gameController) {
        if (GameBoard.isStarted) {
            return;
        }

        if (GameBoard.isStopped) {
            GameBoard.isStopped = false;
            GameBoard.isPaused = false;
            createSnake(gameController);
            createGreenFrogs(gameController);
            createRedFrogs(gameController);
            gameController.resetPoints();

        } else if (GameBoard.isPaused) {
            GameBoard.isPaused = false;
            createSnakeThread(gameController, gameBoard.getSnake());
            createFrogThreads(gameController, gameBoard.getFrogs());
        }
        GameBoard.isStarted = true;
        setChanged();
        notifyObservers(Event.START_GAME);
    }

    /**
     * Stop the game
     */
    public void stopGame() {
        if (!GameBoard.isStopped) {
            GameBoard.isStopped = true;
            GameBoard.isStarted = false;
            if (!GameBoard.isPaused) {
                stopThreads();
            }
            gameBoard.setFrogs(new ArrayList<>());
        }
    }

    /**
     * Pause the game
     */
    public void pauseGame() {
        if (GameBoard.isStopped) {
            return;
        }

        if (!GameBoard.isPaused) {
            GameBoard.isPaused = true;
            GameBoard.isStarted = false;
            stopThreads();
        }
    }
}
